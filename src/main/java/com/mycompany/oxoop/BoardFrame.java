package com.mycompany.oxoop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ASUS
 */
public class BoardFrame extends javax.swing.JFrame {

    private int row;
    private int col;
    JButton btnTables[][] = null;
    Board board;
    Player o;
    Player x;

    /**
     * Creates new form BoardFrame
     */
    public BoardFrame() {
        initComponents();
        initTable();
        initGame();
        showTable();
        showTurn();
        hideNewGame();
        btnNewGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NewGame();
            }

        });

    }

    public void initGame() {
        PlayerService.load();
        o = PlayerService.getO();
        x = PlayerService.getX();
        board = new Board(o, x);
    }

    public void initTable() {
        JButton tables[][] = {{btnTable1, btnTable2, btnTable3},
        {btnTable4, btnTable5, btnTable6},
        {btnTable7, btnTable8, btnTable9}};
        btnTables = tables;
        for (int i = 0; i < btnTables.length; i++) {
            for (int j = 0; j < btnTables[i].length; j++) {
                btnTables[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String strCom = e.getActionCommand();
                        String coms[] = strCom.split(",");
                        row = Integer.parseInt(coms[0]);
                        col = Integer.parseInt(coms[1]);
                        //showRowCol();
                        if (board.setRowCol(row, col)) {
                            showTable();
                            if (board.checkWin()) {
                                showScore();
                                showWin();

                                return;
                            }

                            switchTurn();

                        }

                    }
                });
            }
        }
    }

    public void showRowCol() {
        lblRowCol.setText("Row : " + row + " Col : " + col);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnTable1 = new javax.swing.JButton();
        btnTable2 = new javax.swing.JButton();
        btnTable3 = new javax.swing.JButton();
        btnTable4 = new javax.swing.JButton();
        btnTable5 = new javax.swing.JButton();
        btnTable6 = new javax.swing.JButton();
        btnTable7 = new javax.swing.JButton();
        btnTable8 = new javax.swing.JButton();
        btnTable9 = new javax.swing.JButton();
        lblRowCol = new javax.swing.JLabel();
        btnNewGame = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        ScoreO = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        ScoreX = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnTable1.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable1.setText("-");
        btnTable1.setActionCommand("1,1");
        btnTable1.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable1.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable1.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable2.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable2.setText("-");
        btnTable2.setActionCommand("1,2");
        btnTable2.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable2.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable2.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable3.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable3.setText("-");
        btnTable3.setActionCommand("1,3");
        btnTable3.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable3.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable3.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable4.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable4.setText("-");
        btnTable4.setActionCommand("2,1");
        btnTable4.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable4.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable4.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable5.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable5.setText("-");
        btnTable5.setActionCommand("2,2");
        btnTable5.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable5.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable5.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable6.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable6.setText("-");
        btnTable6.setActionCommand("2,3");
        btnTable6.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable6.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable6.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable7.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable7.setText("-");
        btnTable7.setActionCommand("3,1");
        btnTable7.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable7.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable7.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable8.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable8.setText("-");
        btnTable8.setActionCommand("3,2");
        btnTable8.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable8.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable8.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable9.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable9.setText("-");
        btnTable9.setActionCommand("3,3");
        btnTable9.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable9.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable9.setPreferredSize(new java.awt.Dimension(80, 80));

        lblRowCol.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblRowCol.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblRowCol.setText("Row : ? Col : ?");

        btnNewGame.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnNewGame.setText("New Game");

        jPanel1.setBackground(new java.awt.Color(224, 187, 222));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ScoreO, javax.swing.GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ScoreO, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel2.setBackground(java.awt.Color.pink);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ScoreX, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ScoreX, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnSave.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnNewGame, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblRowCol, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(btnTable4, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnTable5, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnTable6, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(btnTable7, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnTable8, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnTable9, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnTable1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable2, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable3, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(1, 1, 1)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(38, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnTable3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnTable6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnTable9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblRowCol, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnNewGame, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        PlayerService.save();
    }//GEN-LAST:event_btnSaveActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BoardFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BoardFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BoardFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BoardFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BoardFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ScoreO;
    private javax.swing.JLabel ScoreX;
    private javax.swing.JButton btnNewGame;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnTable1;
    private javax.swing.JButton btnTable2;
    private javax.swing.JButton btnTable3;
    private javax.swing.JButton btnTable4;
    private javax.swing.JButton btnTable5;
    private javax.swing.JButton btnTable6;
    private javax.swing.JButton btnTable7;
    private javax.swing.JButton btnTable8;
    private javax.swing.JButton btnTable9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblRowCol;
    // End of variables declaration//GEN-END:variables
    private void hideNewGame() {
        btnNewGame.setVisible(false);
    }

    private void showNewGame() {
        btnNewGame.setVisible(true);
    }

    private void showTable() {
        char data[][] = board.getBoard();
        for (int i = 0; i < btnTables.length; i++) {
            for (int j = 0; j < btnTables[i].length; j++) {
                btnTables[i][j].setText("" + data[i][j]);
            }
        }
    }

    private void showScore() {
        ScoreO.setText("O Win = " + o.getWin() + " Lose = " + o.getLose() + " Draw = " + o.getDraw());
        ScoreX.setText("x Win = " + x.getWin() + " Lose = " + x.getLose() + " Draw = " + x.getDraw());
    }

    private void switchTurn() {
        board.switchPlayer();
        showTurn();
    }

    public void showTurn() {
        lblRowCol.setText("Turn : " + board.getCurrentPlayer().getName());
    }

    private void showWin() {
        if (board.getWinner() == null) {
            lblRowCol.setText("Draw!");
        } else {
            lblRowCol.setText(board.getWinner().getName() + " Win");
        }
        for (int i = 0; i < btnTables.length; i++) {
            for (int j = 0; j < btnTables[i].length; j++) {
                btnTables[i][j].setEnabled(false);
            }
        }
        showNewGame();
    }

    private void NewGame() {
        board = new Board(o, x);
        showTable();
        for (int i = 0; i < btnTables.length; i++) {
            for (int j = 0; j < btnTables[i].length; j++) {
                btnTables[i][j].setEnabled(true);
            }
        }
    }
    

}
